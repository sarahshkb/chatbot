
## interactive_story_1
* greet
    - utter_greet
* finethanks
    - utter_ask_name
* telling_name{"name": "سارا"}
    - slot{"name": "سارا"}
    - utter_ask_age

## interactive_story_1
* greet
    - utter_greet
* thanks_how_about_you
    - utter_fine_thanks
    - utter_ask_name
* telling_name{"name": "سارا"}
    - slot{"name": "سارا"}
    - utter_ask_age
* telling_age{"age": "23"}
    - slot{"age": "23"}
    - utter_ask_job
* telling_job{"job": "برنامه نویسی"}
    - slot{"job": "برنامه نویسی"}
    - utter_ask_mood
* mood_unhappy
