from rasa.core.agent import Agent
from rasa.core.interpreter import RasaNLUInterpreter
import asyncio


async def callFunc():
    await agent.handle_text("اسم من پویاست");


agent = Agent.load("./models/nlu-20200718-163000.tar.gz")
asyncio.run(callFunc())
